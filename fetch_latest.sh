#! /bin/bash

USER=$1
PWD=$2
VERSION=$3


if [[ "$VERSION" != "99-SNAPSHOT" ]]; then
    curl -u ${USR}:${PWS} -O "http://localhost:8082/artifactory/libs-release-local/com/lidar/telemetry/maven-metadata.xml"
    VERSION=$(grep -oPm1 "(?<=<release>)[^<]+" maven-metadata.xml)
fi

mvn dependency:get -Dartifact=com.lidar:telemetry:$VERSION -Ddest=telemetry.jar